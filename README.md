## CISTERNA E IRRIGADOURO

Este é um sistema embarcado `CisternaIrrigadouro` que abrange dois sub sistemas, `Cisterna` e `Irrigadouro`, os dois sistemas foram agrupados para aproveitamento de hardware e compartilhamento de recursos.

### Cisterna

A Cisterna controla o nível de água de uma caixa d'agua de reúso em um piso superior com o bombeamento da água de uma cisterna, há um sensor de nível baixo na cisterna e sensores de níveis alto e baixo na caixa d'agua, quando há água na cisterna e o nível da caixa superior está baixo, o bombeamento é iniciado até que o nível alto seja atingido ou que o nível baixo da cisterna seja ativado.

### Irrigadouro

O Irrigadouro controla o nível de humidade da terra para manter em condições ideais para uma pequeno plantio, há um sensor de humidade do solo e quando atingido nívels de humidade abaixo do esperado o bombeamento de água é iniciado para o solo até que o nível desejado de humidade seja atingido ou o nível baixo da caixa d'água de reúso seja ativado.

### CisternaIrrigadouro

O CisternaIrrigadouro intermedia o controle do sistema gerenciando a inicialização e funcionamento dos dois sub sistemas e uniformizando um mecanismo de informações e alerta.

## ARDUINO MEGA 2560 PINOUT SCHEMA

 * __A0__ cisternLowLevelWaterSensor - input w/ pullout
 * __A1__ waterBoxLowLevelWaterSensor - input w/ pullout
 * __A2__ waterBoxHightLevelWaterSensor - input w/ pullout
 * __A3__ soilMoistureSensor - input
 * __A4__ soilMoistureSelector - input
 * __21__ (start hight) = oLed SLC
 * __20__ (start hight) = oLed SDA
 * __22__ (start hight) = waterBoxPumpRelay - output
 * __23__ (start hight) = irrigationPumpRelay - output
 * __24__ (start hight) = greenLed - output
 * __25__ (start low) = redLed - output

## CONVENÇÃO DE SENSORES
1. __SENSOR DE HUMIDADE:__ O sensor de humidade do solo traz valores de 0 a 1024 sendo 0 totalmente umido e 1024 totalmente seco.
Desta forma o cálculo para porcentagem de nível de humidade se dá através da equação:
`LEITURA_DO_SENSOR = 1024-(1024/100*PORCENTAGEM_DE_HUMIDADE)`
ou, refatorando...
`PORCENTAGEM_DE_HUMIDADE = -(LEITURA_DO_SENSOR-1024)*100/1024`

1. __SENSORES DE NIVEL DE ÁGUA:__ Os sensores de nível de água trazem valores de 0 a 1 na leitura digital, ou de 0 a 1024 na leitura analógica, porém não trazem valores intermediários, e considerando a boia apontada para baixo, 0 indica nível cheio, e 1 indica nível vazio. 
