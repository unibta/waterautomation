#include "Arduino.h"

#include "src/implementations/CisternLowLevelWaterSensorImp.h"
#include "src/implementations/IrrigationPumpRelayImp.h"
#include "src/implementations/LoggerImp.h"
#include "src/implementations/SoilMoistureSensorImp.h"
#include "src/implementations/UtilsImp.h"
#include "src/implementations/WaterBoxLowLevelWaterSensorImp.h"
#include "src/implementations/WaterBoxPumpRelayImp.h"
#include "src/implementations/DisplayImp.h"
#include "src/implementations/SoilMoistureSelectorImp.h"
#include "src/implementations/GreenLedImp.h"
#include "src/implementations/RedLedImp.h"
#include "src/CisternaIrrigador.h"
#include "src/implementations/WaterBoxHighLevelWaterSensorImp.h"

CisternaIrrigador 					* cisternaIrrigador;
CisternLowLevelWaterSensorImp 		* cisternLowLevelWaterSensor;
RedLedImp 							* redLed;
IrrigationPumpRelayImp 				* irrigationPumpRelay;
LoggerImp 							* logger;
SoilMoistureSensorImp 				* soilMoistureSensor;
UtilsImp 							* utils;
WaterBoxHighLevelWaterSensorImp 	* waterBoxHightLevelWaterSensor;
WaterBoxLowLevelWaterSensorImp 		* waterBoxLowLevelWaterSensor;
GreenLedImp 						* greenLed;
WaterBoxPumpRelayImp 				* waterBoxPumpRelay;
SoilMoistureSelectorImp				* soilMoistureSelector;
DisplayImp							* display;

void setup()
{
	cisternLowLevelWaterSensor 		= new CisternLowLevelWaterSensorImp();
	redLed 							= new RedLedImp();
	irrigationPumpRelay 			= new IrrigationPumpRelayImp();
	soilMoistureSensor 				= new SoilMoistureSensorImp();
	utils 							= new UtilsImp();
	waterBoxHightLevelWaterSensor 	= new WaterBoxHighLevelWaterSensorImp();
	waterBoxLowLevelWaterSensor 	= new WaterBoxLowLevelWaterSensorImp();
	greenLed 						= new GreenLedImp();
	waterBoxPumpRelay 				= new WaterBoxPumpRelayImp();
	display 						= new DisplayImp();
	soilMoistureSelector 			= new SoilMoistureSelectorImp();
	logger 							= new LoggerImp();

	cisternaIrrigador = new CisternaIrrigador(display, soilMoistureSelector, cisternLowLevelWaterSensor, redLed, irrigationPumpRelay, logger, soilMoistureSensor, utils, waterBoxHightLevelWaterSensor, waterBoxLowLevelWaterSensor, greenLed, waterBoxPumpRelay);

	logger->logFreeRam();
}

void loop()
{
	cisternaIrrigador->loop();
}
