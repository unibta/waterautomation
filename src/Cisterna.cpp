#include "Cisterna.h"

constexpr char Cisterna::SYSTEM_STATUS_OK[];
constexpr char Cisterna::SYSTEM_STATUS_LOW_CISTERN_WATER_LEVEL[];
constexpr char Cisterna::SYSTEM_STATUS_INCONSISTENT_SENSORS[];

Cisterna::Cisterna(ICisternLowLevelWaterSensor* cisternLowLevelWaterSensor, ILogger* logger, IUtils* utils, IWaterBoxHightLevelWaterSensor* waterBoxHightLevelWaterSensor, IWaterBoxLowLevelWaterSensor* waterBoxLowLevelWaterSensor, IWaterBoxPumpRelay* waterBoxPumpRelay) {
	Cisterna::logger = logger;
	Cisterna::utils = utils;
	Cisterna::cisternLowLevelWaterSensor = cisternLowLevelWaterSensor;
	Cisterna::waterBoxHighLevelWaterSensor = waterBoxHightLevelWaterSensor;
	Cisterna::waterBoxLowLevelWaterSensor = waterBoxLowLevelWaterSensor;
	Cisterna::waterBoxPumpRelay = waterBoxPumpRelay;

	strcpy( systemInfo.errorMessage, SYSTEM_STATUS_OK );
	state = idle_analizing_sensors;
}

Cisterna::~Cisterna() {

}

void Cisterna::loop() {

	readSensorsToSystemInfo();

	switch(state) {
		case idle_analizing_sensors: runIdleAnalizingSensorsState(); break;
		case pumping_water: runPumpingWaterState(); break;
		case alert_cistern_low_level: runAlertCisternLowLevelState(); break;
		case error_inconsistent_sensor_signals: runErrorInconsistentSensorSignalsState(); break;
	}
}

Cisterna::SystemInfo Cisterna::getSystemInfo() {
	return systemInfo;
}

void Cisterna::readSensorsToSystemInfo() {
	systemInfo.isLowLevelCisternTriggered = cisternLowLevelWaterSensor->isTriggered();
	systemInfo.isHighLevelWaterBoxTriggered = waterBoxHighLevelWaterSensor->isTriggered();
	systemInfo.isLowLevelWaterBoxTriggered = waterBoxLowLevelWaterSensor->isTriggered();
	systemInfo.isWaterBoxPumpActive = waterBoxPumpRelay->isOn();
}

void Cisterna::runIdleAnalizingSensorsState() {
	if(isErrorInconsistentSensorSignalsEnterConditionReached()) {
		state = error_inconsistent_sensor_signals;
	}

	else if(isAlertCisternLowLevelEnterConditionReached()) {
		state = alert_cistern_low_level;
	}

	else if(isPumpingWaterEnterConditionReached()) {
		state = pumping_water;
	}
}

void Cisterna::runPumpingWaterState() {
	if(isAlertCisternLowLevelEnterConditionReached()) {
		waterBoxPumpRelay->off();
		state = alert_cistern_low_level;
	}

	else if(isPumpingWaterExitConditionReached()) {
		waterBoxPumpRelay->off();
		state = idle_analizing_sensors;
	}

	else if (!waterBoxPumpRelay->isOn()) {
		waterBoxPumpRelay->on();
	}
}

void Cisterna::runAlertCisternLowLevelState() {
	if(isAlertCisternLowLevelExitConditionReached()) {
		systemInfo.hasErrors = false;
		strcpy( systemInfo.errorMessage, SYSTEM_STATUS_OK );
		state = idle_analizing_sensors;
	}

	else {
		if(!systemInfo.hasErrors){
			systemInfo.hasErrors = true;
			strcpy( systemInfo.errorMessage, SYSTEM_STATUS_LOW_CISTERN_WATER_LEVEL );
		}
	}
}

void Cisterna::runErrorInconsistentSensorSignalsState() {
	if(isErrorInconsistentSensorSignalsExitConditionReached()) {
		systemInfo.hasErrors = false;
		strcpy( systemInfo.errorMessage, SYSTEM_STATUS_OK );
		state = idle_analizing_sensors;
	}

	else {
		if(!systemInfo.hasErrors){
			systemInfo.hasErrors = true;
			strcpy( systemInfo.errorMessage, SYSTEM_STATUS_INCONSISTENT_SENSORS );
		}
	}
}

bool Cisterna::isPumpingWaterEnterConditionReached() {
	return !cisternLowLevelWaterSensor->isTriggered() && waterBoxLowLevelWaterSensor->isTriggered() && waterBoxHighLevelWaterSensor->isTriggered();
}

bool Cisterna::isAlertCisternLowLevelEnterConditionReached() {
	return cisternLowLevelWaterSensor->isTriggered();
}

bool Cisterna::isErrorInconsistentSensorSignalsEnterConditionReached() {
	return waterBoxLowLevelWaterSensor->isTriggered() && !waterBoxHighLevelWaterSensor->isTriggered();
}

bool Cisterna::isPumpingWaterExitConditionReached() {
	return !waterBoxHighLevelWaterSensor->isTriggered();
}

bool Cisterna::isAlertCisternLowLevelExitConditionReached() {
	return !cisternLowLevelWaterSensor->isTriggered();
}

bool Cisterna::isErrorInconsistentSensorSignalsExitConditionReached() {
	return !isErrorInconsistentSensorSignalsEnterConditionReached();
}
