#include "CisternaIrrigador.h"

CisternaIrrigador::CisternaIrrigador(IDisplay * display, ISoilMoistureSelector * soilMoistureSelector, ICisternLowLevelWaterSensor* cisternLowLevelWaterSensor, IRedLed* redLed, IIrrigationPumpRelay* irrigationPumpRelay, ILogger* logger, ISoilMoistureSensor* soilMoistureSensor, IUtils* utils, IWaterBoxHightLevelWaterSensor* waterBoxHightLevelWaterSensor, IWaterBoxLowLevelWaterSensor* waterBoxLowLevelWaterSensor, IGreenLed* greenLed, IWaterBoxPumpRelay* waterBoxPumpRelay) {
	CisternaIrrigador::cisternLowLevelWaterSensor = cisternLowLevelWaterSensor;
	CisternaIrrigador::redLed = redLed;
	CisternaIrrigador::irrigationPumpRelay = irrigationPumpRelay;
	CisternaIrrigador::logger = logger;
	CisternaIrrigador::soilMoistureSensor = soilMoistureSensor;
	CisternaIrrigador::utils = utils;
	CisternaIrrigador::waterBoxHightLevelWaterSensor = waterBoxHightLevelWaterSensor;
	CisternaIrrigador::waterBoxLowLevelWaterSensor = waterBoxLowLevelWaterSensor;
	CisternaIrrigador::greenLed = greenLed;
	CisternaIrrigador::waterBoxPumpRelay = waterBoxPumpRelay;
	CisternaIrrigador::soilMoistureSelector = soilMoistureSelector;
	CisternaIrrigador::display = display;

	CisternaIrrigador::cisterna = new Cisterna(cisternLowLevelWaterSensor, logger, utils, waterBoxHightLevelWaterSensor, waterBoxLowLevelWaterSensor, waterBoxPumpRelay);
	CisternaIrrigador::irrigador = new Irrigador(soilMoistureSelector, irrigationPumpRelay, logger, soilMoistureSensor, utils, waterBoxLowLevelWaterSensor);

}

CisternaIrrigador::~CisternaIrrigador() {

}

void CisternaIrrigador::loop() {

	// execute
	cisterna->loop();
	irrigador->loop();

	// get system info
	cisternaSystemInfo = cisterna->getSystemInfo();
	irrigadorSystemInfo = irrigador->getSystemInfo();

	// keep screen on irrigador if values of setpoint is changing
	if(	irrigadorSystemInfo.soilMoistureSelector > previousSoilMoistureSelectorValue + THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION ||
		irrigadorSystemInfo.soilMoistureSelector < previousSoilMoistureSelectorValue - THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION) {
		previousSoilMoistureSelectorValue = irrigadorSystemInfo.soilMoistureSelector;
		timeSinceLastDisplayScreenSwitch = utils->millis();
		currentDisplayScreen = screen_irrigador;
	}

	// switch screens
	if (utils->millis() - timeSinceLastDisplayScreenSwitch > DISPLAY_SCREEN_SWITCH_DELAY) {
		timeSinceLastDisplayScreenSwitch = utils->millis();

		if(currentDisplayScreen==screen_cisterna) {
			currentDisplayScreen = screen_irrigador;
		} else {
			currentDisplayScreen = screen_cisterna;
		}
	}

	updateScreen();

	updateLeds();

	utils->delay(50);
}

void CisternaIrrigador::updateLeds() {
	if(cisternaSystemInfo.hasErrors || irrigadorSystemInfo.hasErrors) {

		if(greenLed->isOn()) {
			greenLed->off();
		}

		if (utils->millis() - timeSinceLastRedLedSwitch > LED_ERROR_TOGLE_DELAY_IN_MS) {
			timeSinceLastRedLedSwitch = utils->millis();
			redLed->toggle();
		}

	} else {

		if(!greenLed->isOn()) {
			greenLed->on();
		}

		if(redLed->isOn()) {
			redLed->off();
		}
	}
}

void CisternaIrrigador::updateScreen() {
	switch(currentDisplayScreen) {
	case screen_cisterna:
		display->displayCisternInfo(cisternaSystemInfo.isLowLevelCisternTriggered, cisternaSystemInfo.isLowLevelWaterBoxTriggered, cisternaSystemInfo.isHighLevelWaterBoxTriggered, cisternaSystemInfo.isWaterBoxPumpActive, cisternaSystemInfo.errorMessage);
	break;
	case screen_irrigador:
		display->displayIrrigationInfo(irrigadorSystemInfo.soilMoistureValue, irrigadorSystemInfo.soilMoistureSelector, irrigadorSystemInfo.isIrrigationPumpActive, irrigadorSystemInfo.errorMessage);
	break;
	}
}
