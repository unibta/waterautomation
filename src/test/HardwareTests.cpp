#include "HardwareTests.h"
#include "Arduino.h"

HardwareTests::HardwareTests(DisplayImp * display, SoilMoistureSelectorImp * soilMoistureSelector, CisternLowLevelWaterSensorImp* cisternLowLevelWaterSensor, RedLedImp* redLed, IrrigationPumpRelayImp* irrigationPumpRelay, LoggerImp* logger, SoilMoistureSensorImp* soilMoistureSensor, UtilsImp* utils, WaterBoxHighLevelWaterSensorImp* waterBoxHightLevelWaterSensor, WaterBoxLowLevelWaterSensorImp* waterBoxLowLevelWaterSensor, GreenLedImp* greenLed, WaterBoxPumpRelayImp* waterBoxPumpRelay) {
	HardwareTests::cisternLowLevelWaterSensor = cisternLowLevelWaterSensor;
	HardwareTests::redLed = redLed;
	HardwareTests::irrigationPumpRelay = irrigationPumpRelay;
	HardwareTests::logger = logger;
	HardwareTests::soilMoistureSensor = soilMoistureSensor;
	HardwareTests::utils = utils;
	HardwareTests::waterBoxHightLevelWaterSensor = waterBoxHightLevelWaterSensor;
	HardwareTests::waterBoxLowLevelWaterSensor = waterBoxLowLevelWaterSensor;
	HardwareTests::greenLed = greenLed;
	HardwareTests::waterBoxPumpRelay = waterBoxPumpRelay;
	HardwareTests::soilMoistureSelector = soilMoistureSelector;
	HardwareTests::display = display;

	Serial.begin(9600);

}

HardwareTests::~HardwareTests() {

}

void HardwareTests::run() {

	delay(DELAY_BETWEEN_TESTS);
	Serial.println("CISTERNA IRRIGADOURO TESTS");

//	displayTest();
//	delay(DELAY_BETWEEN_TESTS);

//	greenLedTest();
//	delay(DELAY_BETWEEN_TESTS);
//
//	redLedTest();
//	delay(DELAY_BETWEEN_TESTS);
//
//	Serial.println("Switch water level sensors to empty.");
//	delay(DELAY_FOR_HARDWARE_SETUP);
//	cisternLowLevelWaterSensorTest();
//	waterBoxLowLevelWaterSensorTest();
//	waterBoxHightLevelWaterSensorTest();
//	delay(DELAY_BETWEEN_TESTS);
//
//	Serial.println("Switch water level sensors to full.");
//	delay(DELAY_FOR_HARDWARE_SETUP);
//	cisternLowLevelWaterSensorTest();
//	waterBoxLowLevelWaterSensorTest();
//	waterBoxHightLevelWaterSensorTest();
//	delay(DELAY_BETWEEN_TESTS);
//
	soilMoistureSensorTest();
	delay(DELAY_BETWEEN_TESTS);
//
//	soilMoistureSelectorTest();
//	delay(DELAY_BETWEEN_TESTS);
//
//	waterBoxPumpRelayTest();
//	delay(DELAY_BETWEEN_TESTS);
//
//	irrigationPumpRelayTest();
//	delay(DELAY_BETWEEN_TESTS);

	Serial.println("ALL TESTS DONE ===");
}

void HardwareTests::cisternLowLevelWaterSensorTest() {
	Serial.println("cisternLowLevelWaterSensorTest");
	for(int i=0; i<4; ++i){
		readableTest(cisternLowLevelWaterSensor);
		delay(4000);
	}
}

void HardwareTests::waterBoxLowLevelWaterSensorTest(){
	Serial.println("waterBoxLowLevelWaterSensorTest");
	for(int i=0; i<4; ++i){
		readableTest(waterBoxLowLevelWaterSensor);
		delay(4000);
	}
}

void HardwareTests::waterBoxHightLevelWaterSensorTest() {
	Serial.println("waterBoxHightLevelWaterSensorTest");
	for(int i=0; i<4; ++i){
		readableTest(waterBoxHightLevelWaterSensor);
		delay(4000);
	}
}

void HardwareTests::redLedTest() {
	Serial.println("redLedTest");
	switchableTest(redLed);
}

void HardwareTests::irrigationPumpRelayTest() {
	Serial.println("irrigationPumpRelayTest");
	switchableTest(irrigationPumpRelay);
}

void HardwareTests::soilMoistureSensorTest() {
	Serial.println("soilMoistureSensorTest");
	for(int i=0; i<100; ++i){
		readableTest(soilMoistureSensor);
		delay(500);
	}
}

void HardwareTests::greenLedTest() {
	Serial.println("greenLedTest");
	switchableTest(greenLed);
}

void HardwareTests::waterBoxPumpRelayTest() {
	Serial.println("waterBoxPumpRelayTest");
	switchableTest(waterBoxPumpRelay);
}

void HardwareTests::displayTest() {
	Serial.println("displayTest");
	display->displayCisternInfo(true, true, true, false, "Low levels");
	delay(3000);
	display->displayIrrigationInfo(1023, 1023, true, "High levels");
	delay(3000);
}

void HardwareTests::soilMoistureSelectorTest() {
	Serial.println("soilMoistureSelectorTest");
	for(int i=0; i<4; ++i){
		readableTest(soilMoistureSelector);
		delay(4000);
	}
}

void HardwareTests::readableTest(IReadable * readable) {
	Serial.print("- sensor value: ");
	int value = readable->readValue();
	Serial.println(value);

	Serial.print("- sensor percent: ");
	int percentage = readable->readValuePercentage();
	Serial.println(percentage);

	Serial.print("- sensor is triggered: ");
	bool isTriggered = readable->isTriggered();
	Serial.println(isTriggered);
}

void HardwareTests::switchableTest(ISwitchable * switchable) {
	Serial.println("- switched on");
	switchable->on();
	delay(1000);
	Serial.println("- switched off");
	switchable->off();
}
