#ifndef UNITTESTS_H_
#define UNITTESTS_H_

#include "Arduino.h"

#include "../implementations/CisternLowLevelWaterSensorImp.h"
#include "../implementations/SoilMoistureSensorImp.h"
#include "../implementations/WaterBoxLowLevelWaterSensorImp.h"
#include "../implementations/RedLedImp.h"
#include "../implementations/LoggerImp.h"
#include "../implementations/UtilsImp.h"
#include "../implementations/GreenLedImp.h"
#include "../implementations/IrrigationPumpRelayImp.h"
#include "../implementations/WaterBoxPumpRelayImp.h"
#include "../implementations/SoilMoistureSelectorImp.h"
#include "../implementations/DisplayImp.h"

//#include "../implementations/MultiplexedSensors.h"
#include "../implementations/WaterBoxHighLevelWaterSensorImp.h"

class HardwareTests {
	public:
		HardwareTests(DisplayImp*, SoilMoistureSelectorImp*, CisternLowLevelWaterSensorImp*, RedLedImp*, IrrigationPumpRelayImp*, LoggerImp*, SoilMoistureSensorImp*, UtilsImp*, WaterBoxHighLevelWaterSensorImp*, WaterBoxLowLevelWaterSensorImp*, GreenLedImp*, WaterBoxPumpRelayImp*);
		virtual ~HardwareTests();

		void run();

	private:

		const int DELAY_BETWEEN_TESTS = 2000;
		const int DELAY_FOR_HARDWARE_SETUP = 10000;

		CisternLowLevelWaterSensorImp 		* cisternLowLevelWaterSensor;
		RedLedImp 							* redLed;
		IrrigationPumpRelayImp 				* irrigationPumpRelay;
		LoggerImp 							* logger;
		SoilMoistureSensorImp 				* soilMoistureSensor;
		UtilsImp 							* utils;
		WaterBoxHighLevelWaterSensorImp 	* waterBoxHightLevelWaterSensor;
		WaterBoxLowLevelWaterSensorImp 		* waterBoxLowLevelWaterSensor;
		GreenLedImp 						* greenLed;
		WaterBoxPumpRelayImp 				* waterBoxPumpRelay;
		SoilMoistureSelectorImp				* soilMoistureSelector;
		DisplayImp							* display;

		void displayTest();
		void cisternLowLevelWaterSensorTest();
		void redLedTest();
		void irrigationPumpRelayTest();
		void soilMoistureSensorTest();
		void soilMoistureSelectorTest();
		void waterBoxHightLevelWaterSensorTest();
		void waterBoxLowLevelWaterSensorTest();
		void greenLedTest();
		void waterBoxPumpRelayTest();

		void readableTest(IReadable * readable);
		void switchableTest(ISwitchable * switchable);
};

#endif
