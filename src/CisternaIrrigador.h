#ifndef CISTERNAIRRIGADOR_H_
#define CISTERNAIRRIGADOR_H_

#include "interfaces/ICisternLowLevelWaterSensor.h"
#include "interfaces/IIrrigationPumpRelay.h"
#include "interfaces/ILogger.h"
#include "interfaces/ISoilMoistureSensor.h"
#include "interfaces/IUtils.h"
#include "interfaces/IWaterBoxHightLevelWaterSensor.h"
#include "interfaces/IWaterBoxLowLevelWaterSensor.h"
#include "interfaces/IWaterBoxPumpRelay.h"
#include "interfaces/IDisplay.h"
#include "interfaces/ISoilMoistureSelector.h"

#include "Cisterna.h"
#include "interfaces/IGreenLed.h"
#include "interfaces/IRedLed.h"
#include "Irrigador.h"

class CisternaIrrigador {
public:
	CisternaIrrigador(IDisplay*, ISoilMoistureSelector*, ICisternLowLevelWaterSensor*, IRedLed*, IIrrigationPumpRelay*, ILogger*, ISoilMoistureSensor*, IUtils*, IWaterBoxHightLevelWaterSensor*, IWaterBoxLowLevelWaterSensor*, IGreenLed*, IWaterBoxPumpRelay*);
	virtual ~CisternaIrrigador();

	void loop();

private:

	const int THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION = 50; // about 5% of 1024 (1024*0.05)
	const long DISPLAY_SCREEN_SWITCH_DELAY = 10000;
	const int RUN_LOOP_DELAY_IN_MS = 1000;
	const int LED_ERROR_TOGLE_DELAY_IN_MS = 1000;

	enum ScreenMode {
		screen_cisterna,
		screen_irrigador
	};

	ICisternLowLevelWaterSensor 	* cisternLowLevelWaterSensor;
	IRedLed 						* redLed;
	IIrrigationPumpRelay 			* irrigationPumpRelay;
	ILogger 						* logger;
	ISoilMoistureSensor 			* soilMoistureSensor;
	IUtils 							* utils;
	IWaterBoxHightLevelWaterSensor 	* waterBoxHightLevelWaterSensor;
	IWaterBoxLowLevelWaterSensor 	* waterBoxLowLevelWaterSensor;
	IGreenLed 						* greenLed;
	IWaterBoxPumpRelay 				* waterBoxPumpRelay;
	ISoilMoistureSelector			* soilMoistureSelector;
	IDisplay						* display;

	Cisterna 	* cisterna;
	Irrigador 	* irrigador;

	Cisterna::SystemInfo cisternaSystemInfo;
	Irrigador::SystemInfo irrigadorSystemInfo;

	int previousSoilMoistureSelectorValue = 0;
	long timeSinceLastDisplayScreenSwitch = 0;
	long timeSinceLastRedLedSwitch = 0;
	ScreenMode currentDisplayScreen = screen_cisterna;

	void updateLeds();
	void updateScreen();
};

#endif
