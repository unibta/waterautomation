/*
 * Cisterna.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef CISTERNA_H_
#define CISTERNA_H_

#include "interfaces/ICisternLowLevelWaterSensor.h"
#include "interfaces/ILogger.h"
#include "interfaces/IUtils.h"
#include "interfaces/IWaterBoxHightLevelWaterSensor.h"
#include "interfaces/IWaterBoxLowLevelWaterSensor.h"
#include "interfaces/IWaterBoxPumpRelay.h"

#include <string.h>

class Cisterna {
public:
	Cisterna(ICisternLowLevelWaterSensor*, ILogger*, IUtils*, IWaterBoxHightLevelWaterSensor*, IWaterBoxLowLevelWaterSensor*, IWaterBoxPumpRelay*);
	virtual ~Cisterna();

	static constexpr const int MAX_STRING_LENGTH = 30;

	struct SystemInfo {
		int isLowLevelCisternTriggered;
		int isLowLevelWaterBoxTriggered;
		int isHighLevelWaterBoxTriggered;
		bool isWaterBoxPumpActive;
		bool hasErrors = false;
		char errorMessage[MAX_STRING_LENGTH];
	};

	void loop();
	SystemInfo getSystemInfo();

private:

	static constexpr char SYSTEM_STATUS_OK[] 						= ":) cisterna ok";
	static constexpr char SYSTEM_STATUS_LOW_CISTERN_WATER_LEVEL[] 	= "[!] nivel baixo";
	static constexpr char SYSTEM_STATUS_INCONSISTENT_SENSORS[] 		= "[!] erro sensores";

	enum State {
		idle_analizing_sensors,
		pumping_water,
		alert_cistern_low_level,
		error_inconsistent_sensor_signals
	};

	ICisternLowLevelWaterSensor * cisternLowLevelWaterSensor;
	ILogger * logger;
	IUtils * utils;
	IWaterBoxHightLevelWaterSensor * waterBoxHighLevelWaterSensor;
	IWaterBoxLowLevelWaterSensor * waterBoxLowLevelWaterSensor;
	IWaterBoxPumpRelay * waterBoxPumpRelay;

	State state;
	SystemInfo systemInfo;

	void readSensorsToSystemInfo();

	void runIdleAnalizingSensorsState();
	void runPumpingWaterState();
	void runAlertCisternLowLevelState();
	void runErrorInconsistentSensorSignalsState();

	bool isPumpingWaterEnterConditionReached();
	bool isAlertCisternLowLevelEnterConditionReached();
	bool isErrorInconsistentSensorSignalsEnterConditionReached();

	bool isPumpingWaterExitConditionReached();
	bool isAlertCisternLowLevelExitConditionReached();
	bool isErrorInconsistentSensorSignalsExitConditionReached();
};

#endif /* CISTERNA_H_ */
