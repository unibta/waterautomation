#ifndef IRRIGADOR_H_
#define IRRIGADOR_H_

#include "interfaces/IIrrigationPumpRelay.h"
#include "interfaces/ILogger.h"
#include "interfaces/IRedLed.h"
#include "interfaces/ISoilMoistureSensor.h"
#include "interfaces/ISoilMoistureSelector.h"
#include "interfaces/IUtils.h"
#include "interfaces/IWaterBoxLowLevelWaterSensor.h"

#include <string.h>

class Irrigador {
public:
	Irrigador(ISoilMoistureSelector * soilMoistureSelector, IIrrigationPumpRelay*, ILogger*, ISoilMoistureSensor*, IUtils*, IWaterBoxLowLevelWaterSensor*);
	virtual ~Irrigador();

	static constexpr const int MAX_STRING_LENGTH = 128;

	struct SystemInfo {
		int isLowLevelWaterBoxTriggered;
		int soilMoistureValue;
		int soilMoistureSelector;
		bool isIrrigationPumpActive;
		bool hasErrors = false;
		char errorMessage[128];
	};

	void loop();
	SystemInfo getSystemInfo();

private:

	static constexpr char SYSTEM_STATUS_OK[] 				= "=) irrigador ok";
	static constexpr char SYSTEM_STATUS_LOW_WATER_LEVEL[] 	= "[!] nivel baixo";
	const int THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION = 15; // about 5% of 1024 (1024*0.05)

	enum State {
		idle_analizing_sensors,
		pumping_water,
		alert_waterbox_low_level
	};

	ILogger * logger;
	IUtils * utils;
	IIrrigationPumpRelay * irrigationPumpRelay;
	ISoilMoistureSensor * soilMoistureSensor;
	ISoilMoistureSelector * soilMoistureSelector;
	IWaterBoxLowLevelWaterSensor * waterBoxLowLevelWaterSensor;
	State state;
	SystemInfo systemInfo;

	void readSensorsToSystemInfo();

	void runIdleAnalizingSensorsState();
	void runPumpingWaterState();
	void runAlertWaterboxLowLevelState();

	bool isPumpingWaterEnterConditionReached();
	bool isAlertWaterboxLowLevelEnterConditionReached();

	bool isPumpingWaterExitConditionReached();
	bool isAlertWaterboxLowLevelExitConditionReached();
};

#endif
