/*
 * IWaterBoxPumpRelay.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IWATERBOXPUMPRELAY_H_
#define INTERFACES_IWATERBOXPUMPRELAY_H_

#include "ISwitchable.h"

class IWaterBoxPumpRelay: virtual public ISwitchable {

};

#endif /* INTERFACES_IWATERBOXPUMPRELAY_H_ */
