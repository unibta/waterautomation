#ifndef INTERFACES_ILOGGER_H_
#define INTERFACES_ILOGGER_H_

class ILogger {
	public:

		virtual void logFreeRam() = 0;

		virtual void log(const char c[]) = 0;
		virtual void log(const char c) = 0;
		virtual void log(int num) = 0;
		virtual void log(long num) = 0;
		virtual void log(double num) = 0;
		virtual void log(bool b) = 0;

		virtual void logln(const char c[]) = 0;
		virtual void logln(const char c) = 0;
		virtual void logln(int num) = 0;
		virtual void logln(long num) = 0;
		virtual void logln(double num) = 0;
		virtual void logln(bool b) = 0;
};

#endif
