/*
 * IWaterBoxLowLevelWaterSensor.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IWATERBOXLOWLEVELWATERSENSOR_H_
#define INTERFACES_IWATERBOXLOWLEVELWATERSENSOR_H_

#include "IReadable.h"

class IWaterBoxLowLevelWaterSensor: virtual public IReadable {

};

#endif /* INTERFACES_IWATERBOXLOWLEVELWATERSENSOR_H_ */
