/*
 * IIrrigationPumpOnLed.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IREDLED_H_
#define INTERFACES_IREDLED_H_

#include "ISwitchable.h"

class IRedLed: virtual public ISwitchable {

};

#endif /* INTERFACES_IREDLED_H_ */
