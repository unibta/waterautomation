/*
 * IDisplay.h
 *
 *  Created on: Oct 24, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IDISPLAY_H_
#define INTERFACES_IDISPLAY_H_

class IDisplay {
public:
	virtual void displayCisternInfo(const bool isLowLevelCisternTriggered, const bool isLowLevelWaterBoxTriggered, const bool isHighLevelWaterBoxTriggered, const bool isWaterBoxPumpActive, const char errorMessage[128]);
	virtual void displayIrrigationInfo(const int soilMoistureValue, const int soilMoistureSetpoint, const bool isIrrigationPumpActive, const char errorMessage[128]);
};

#endif /* INTERFACES_IDISPLAY_H_ */
