#ifndef INTERFACES_IUTILS_H_
#define INTERFACES_IUTILS_H_

class IUtils {
	public:
//		virtual ~IUtils();
		virtual void delay(unsigned long ms) = 0;
		virtual bool isDigit(const char c) = 0;
		virtual unsigned long millis() = 0;
};

#endif

