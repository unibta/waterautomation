/*
 * ICisternLowLevelWaterSensor.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_ICISTERNLOWLEVELWATERSENSOR_H_
#define INTERFACES_ICISTERNLOWLEVELWATERSENSOR_H_

#include "IReadable.h"

class ICisternLowLevelWaterSensor: virtual public IReadable {
public:
	//virtual ~ICisternLowLevelWaterSensor();
};

#endif /* INTERFACES_ICISTERNLOWLEVELWATERSENSOR_H_ */
