/*
 * IIrrigationPumpRelay.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IIRRIGATIONPUMPRELAY_H_
#define INTERFACES_IIRRIGATIONPUMPRELAY_H_

#include "ISwitchable.h"

class IIrrigationPumpRelay: virtual public ISwitchable {

};

#endif /* INTERFACES_IIRRIGATIONPUMPRELAY_H_ */
