/*
 * IWaterBoxHightLevelWaterSensor.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IWATERBOXHIGHTLEVELWATERSENSOR_H_
#define INTERFACES_IWATERBOXHIGHTLEVELWATERSENSOR_H_

#include "IReadable.h"

class IWaterBoxHightLevelWaterSensor: virtual public IReadable {

};

#endif /* INTERFACES_IWATERBOXHIGHTLEVELWATERSENSOR_H_ */
