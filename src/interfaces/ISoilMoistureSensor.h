/*
 * ISoilMoistureSensor.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_ISOILMOISTURESENSOR_H_
#define INTERFACES_ISOILMOISTURESENSOR_H_

#include "IReadable.h"

class ISoilMoistureSensor: virtual public IReadable {

};

#endif /* INTERFACES_ISOILMOISTURESENSOR_H_ */
