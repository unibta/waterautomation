/*
 * ISwitchable.h
 *
 *  Created on: Oct 16, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_ISWITCHABLE_H_
#define INTERFACES_ISWITCHABLE_H_

class ISwitchable {
public:

	virtual void on() = 0;
	virtual void off() = 0;
	virtual void toggle() = 0;
	virtual void dim(int dimVal) = 0;
	virtual bool isOn() = 0;
	virtual void setState(bool state) = 0;
};

#endif /* INTERFACES_ISWITCHABLE_H_ */
