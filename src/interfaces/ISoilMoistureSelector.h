/*
 * ISoilMoistureSelector.h
 *
 *  Created on: Oct 24, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_ISOILMOISTURESELECTOR_H_
#define INTERFACES_ISOILMOISTURESELECTOR_H_

#include "IReadable.h"

class ISoilMoistureSelector: virtual public IReadable {

};

#endif /* INTERFACES_ISOILMOISTURESELECTOR_H_ */
