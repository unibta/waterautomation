/*
 * IWaterBoxPumpOnLed.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef INTERFACES_IGREENLED_H_
#define INTERFACES_IGREENLED_H_

#include "ISwitchable.h"

class IGreenLed: virtual public ISwitchable {

};

#endif /* INTERFACES_IGREENLED_H_ */
