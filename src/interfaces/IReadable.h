#ifndef INTERFACES_IREADABLE_H_
#define INTERFACES_IREADABLE_H_

class IReadable {
public:

	virtual int readValue() = 0;
	virtual int readValuePercentage() = 0;
	virtual bool isTriggered() = 0;
	virtual void setThresholdValueToSwitchTriggeredStatus(int value) = 0;

};

#endif
