/*
 * WaterBoxPumpOnLedImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_GREENLEDIMP_H_
#define IMPLEMENTATIONS_GREENLEDIMP_H_

#include "Arduino.h"

#include "../interfaces/IGreenLed.h"
#include "SwitchableImp.h"

class GreenLedImp: virtual public IGreenLed, public SwitchableImp {
public:
	GreenLedImp();
	virtual ~GreenLedImp();

private:
	const uint8_t PIN_SIGNAL = 24;
};

#endif /* IMPLEMENTATIONS_GREENLEDIMP_H_ */
