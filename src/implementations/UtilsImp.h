/*
 * UtilsImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_UTILSIMP_H_
#define IMPLEMENTATIONS_UTILSIMP_H_

#include "Arduino.h"
#include "../interfaces/IUtils.h"

class UtilsImp: virtual public IUtils {
public:
	UtilsImp();
	virtual ~UtilsImp();

	void delay(unsigned long ms);
	bool isDigit(const char c);
	unsigned long millis();
};

#endif /* IMPLEMENTATIONS_UTILSIMP_H_ */
