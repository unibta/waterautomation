/*
 * IrrigationPumpRelayImp.cpp
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#include "IrrigationPumpRelayImp.h"

IrrigationPumpRelayImp::IrrigationPumpRelayImp() {
	setPin(PIN_SIGNAL, true);
}

IrrigationPumpRelayImp::~IrrigationPumpRelayImp() {

}
