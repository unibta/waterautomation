/*
 * SwitchableImpImp.cpp
 *
 *  Created on: Oct 16, 2018
 *      Author: davidlima
 */

#include "SwitchableImp.h"

SwitchableImp::SwitchableImp()
{

}

SwitchableImp::SwitchableImp(const uint8_t pin)
{
    setPin(pin); // @suppress("Ambiguous problem")
}

SwitchableImp::SwitchableImp(const uint8_t pin, bool inverseOutput)
{
    setPin(pin, inverseOutput);
}

SwitchableImp::~SwitchableImp()
{

}

void SwitchableImp::setPin(const uint8_t pin) {
	SwitchableImp::pin = pin;
	pinMode(pin, OUTPUT);
	off();
}

void SwitchableImp::setPin(const uint8_t pin, bool inverseOutput) {
	SwitchableImp::is_output_inverted = inverseOutput;
	setPin(pin); // @suppress("Ambiguous problem")
}

void SwitchableImp::updateOutputState() {
	if(state == true){
		on();
	} else {
		off();
	}
}

void SwitchableImp::on()
{
	state = true;
	digitalWrite(pin, is_output_inverted ? LOW : HIGH);
}

void SwitchableImp::off()
{
	state = false;
	digitalWrite(pin, is_output_inverted ? HIGH : LOW);
}

void SwitchableImp::toggle()
{
	state = !state;
	updateOutputState();
}

void SwitchableImp::dim(int dimVal)
{
    analogWrite(pin, dimVal);
}

bool SwitchableImp::isOn()
{
    return state;
}

void SwitchableImp::setState(bool state)
{
	SwitchableImp::state = state;
	updateOutputState();
}
