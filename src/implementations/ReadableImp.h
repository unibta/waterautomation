#ifndef IMPLEMENTATIONS_READABLEIMP_H_
#define IMPLEMENTATIONS_READABLEIMP_H_

#include "../interfaces/IReadable.h"
#include <Arduino.h>

class ReadableImp: virtual public IReadable {
public:
	ReadableImp();
	ReadableImp(const uint8_t pin, bool isAnalog, int thresholdValueToSwitchTriggeredStatus, int maxReadableValue, bool enablePullUp);
	virtual ~ReadableImp();

	int readValue();
	int readValuePercentage();
	bool isTriggered();
	void setThresholdValueToSwitchTriggeredStatus(int value);

	void setupPinDefinitions(const uint8_t pin, bool isAnalog, int thresholdValueToSwitchTriggeredStatus, int maxReadableValue, bool enablePullUp);
	void applyPinDefinitions();

protected:

	void setPin(const uint8_t pin, bool enablePullUp);
	void setInputTypeAsAnalog(bool value);
	void setMaxReadableValue(int value);
	void enableABCMultiplexMode(const uint8_t chanelAPin, const uint8_t chanelBPin, const uint8_t chanelCPin, const uint8_t chanelSelectionBits[3]);

private:

	const int THRESHOLD_MARGIN_TO_AVOID_ANALOG_TRIGGER_FLUCTUATION = 50; // about 5% of 1023 (1023*0.05)
	bool currentTriggeredStatusToSolveAnalogFluctuationTriggerFluctuation = false;

	int thresholdValueToSwitchTriggeredStatus = 512;
	int maxReadableValue = 1023;
	bool isAnalog = false;
	bool enablePullUp;
	uint8_t pin;

	bool isPinMultiplexed = false;
	uint8_t chanelAPin;
	uint8_t chanelBPin;
	uint8_t chanelCPin;
	uint8_t chanelSelectionBits[3];

	int analogSensorValue = 0;
	const float EXPONENTIAL_MOVING_AVERAGE_ALPHA = 0.5;
	float exponentialMovingAverageOutput = 0.0;

	void configureMultiplexedChannel();
};

#endif
