#ifndef IMPLEMENTATIONS_CISTERNLOWLEVELWATERSENSORIMP_H_
#define IMPLEMENTATIONS_CISTERNLOWLEVELWATERSENSORIMP_H_

#include "Arduino.h"
#include "../interfaces/ICisternLowLevelWaterSensor.h"
#include "ReadableImp.h"

class CisternLowLevelWaterSensorImp: virtual public ICisternLowLevelWaterSensor, public ReadableImp {
public:
	CisternLowLevelWaterSensorImp();
	virtual ~CisternLowLevelWaterSensorImp();

private:
	const uint8_t PIN_SIGNAL = A0;
	const bool PIN_SIGNAL_IS_ANALOG = true;
	const bool ENABLE_PIN_PULLUP = true;
	const int MAX_READABLE_VALUE = 1023;
	const int THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS = 512;
};

#endif
