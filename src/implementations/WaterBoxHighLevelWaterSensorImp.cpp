#include "WaterBoxHighLevelWaterSensorImp.h"

WaterBoxHighLevelWaterSensorImp::WaterBoxHighLevelWaterSensorImp() {
	setupPinDefinitions(PIN_SIGNAL, PIN_SIGNAL_IS_ANALOG, THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS, MAX_READABLE_VALUE, ENABLE_PIN_PULLUP);
}

WaterBoxHighLevelWaterSensorImp::~WaterBoxHighLevelWaterSensorImp() {

}
