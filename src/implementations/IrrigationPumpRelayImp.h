/*
 * IrrigationPumpRelayImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_IRRIGATIONPUMPRELAYIMP_H_
#define IMPLEMENTATIONS_IRRIGATIONPUMPRELAYIMP_H_

#include "Arduino.h"
#include "../interfaces/IIrrigationPumpRelay.h"
#include "SwitchableImp.h"

class IrrigationPumpRelayImp: virtual public IIrrigationPumpRelay, public SwitchableImp {
public:
	IrrigationPumpRelayImp();
	virtual ~IrrigationPumpRelayImp();

private:
	const uint8_t PIN_SIGNAL = 23;
};

#endif /* IMPLEMENTATIONS_IRRIGATIONPUMPRELAYIMP_H_ */
