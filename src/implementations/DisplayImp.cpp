#include "DisplayImp.h"

DisplayImp::DisplayImp() {

	display = new Adafruit_SSD1306(-1); // OLED_RESET PIN // @suppress("Abstract class cannot be instantiated")
	display->begin(SSD1306_SWITCHCAPVCC, 0x3c, false);  // initialize with the I2C addr 0x3D (for the 128x64)
	display->setRotation(2);
	display->setTextWrap(false);
	display->clearDisplay();
}

DisplayImp::~DisplayImp() {

}

void DisplayImp::displayCisternInfo(const bool isLowLevelCisternTriggered, const bool isLowLevelWaterBoxTriggered, const bool isHighLevelWaterBoxTriggered, const bool isWaterBoxPumpActive, const char errorMessage[128]) {

	display->clearDisplay();
	display->setTextColor(WHITE);
	display->setTextSize(1);

	display->setCursor(0,0);
	display->print(F("CTRL DE CISTERNA"));
	display->drawFastHLine(0, 11, 128, WHITE);

	display->setCursor(0, 14);
	display->print(isLowLevelCisternTriggered ? F("-cisterna: VAZIA") : F("-cisterna: NORMAL"));

	display->setCursor(0, 25);
	display->print(isLowLevelWaterBoxTriggered ? F("-caixa: VAZIA") : (isHighLevelWaterBoxTriggered ? F("-caixa: NORMAL") : F("-caixa: CHEIA")) );

	display->setCursor(0, 36);
	display->print(isWaterBoxPumpActive ? F("-bombeamento: ATIVO") : F("-bombeamento: INATIVO"));

	display->drawFastHLine(0, 47, 128, WHITE);

	display->setCursor(0, 50);
	display->print(errorMessage);

	display->display();
}

void DisplayImp::displayIrrigationInfo(const int soilMoistureValue, const int soilMoistureSetpoint, const bool isIrrigationPumpActive, const char * errorMessage) {

	display->clearDisplay();
	display->setTextColor(WHITE);
	display->setTextSize(1);

	display->setCursor(0,0);
	display->print(F("CTRL DE IRRIGACAO"));
	display->drawFastHLine(0, 11, 128, WHITE);


	sprintf(soilMoistureValueString,"%d", getPercentageBySensorValue(soilMoistureValue));
	strcpy(umidadeSolo, "-umidade solo: ");
	strncat(umidadeSolo, soilMoistureValueString, 3);
	strncat(umidadeSolo, "%", 1);
	display->setCursor(0, 14);
	display->print(umidadeSolo);


	sprintf(soilMoistureSetpointString,"%d", getPercentageBySensorValue(soilMoistureSetpoint));
	strcpy(umidadeMinima, "-umidade minima: ");
	strncat(umidadeMinima, soilMoistureSetpointString, 3);
	strncat(umidadeMinima, "%", 1);
	display->setCursor(0, 25);
	display->print(umidadeMinima);


	display->setCursor(0, 36);
	display->print((isIrrigationPumpActive) ? F("-bombeamento: ATIVO") : F("-bombeamento: INATIVO"));
	display->drawFastHLine(0, 47, 128, WHITE);

	display->setCursor(0, 50);
	display->print(errorMessage);

	display->display();
}

int DisplayImp::getPercentageBySensorValue(int sensorValue) {
	return (int)(((float)sensorValue/1023.0F)*100.0F);
}
