/*
 * WaterBoxPumpRelayImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_WATERBOXPUMPRELAYIMP_H_
#define IMPLEMENTATIONS_WATERBOXPUMPRELAYIMP_H_

#include "Arduino.h"
#include "../interfaces/IWaterBoxPumpRelay.h"
#include "SwitchableImp.h"

class WaterBoxPumpRelayImp: virtual public IWaterBoxPumpRelay, public SwitchableImp {
public:
	WaterBoxPumpRelayImp();
	virtual ~WaterBoxPumpRelayImp();

private:
	const uint8_t PIN_SIGNAL = 22;
};

#endif /* IMPLEMENTATIONS_WATERBOXPUMPRELAYIMP_H_ */
