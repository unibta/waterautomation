#ifndef IMPLEMENTATIONS_WATERBOXLOWLEVELWATERSENSORIMP_H_
#define IMPLEMENTATIONS_WATERBOXLOWLEVELWATERSENSORIMP_H_

#include "Arduino.h"
#include "../interfaces/IWaterBoxLowLevelWaterSensor.h"
#include "ReadableImp.h"

class WaterBoxLowLevelWaterSensorImp: virtual public IWaterBoxLowLevelWaterSensor, public ReadableImp {
public:
	WaterBoxLowLevelWaterSensorImp();
	virtual ~WaterBoxLowLevelWaterSensorImp();

private:
	const uint8_t PIN_SIGNAL = A1;
	const bool PIN_SIGNAL_IS_ANALOG = true;
	const bool ENABLE_PIN_PULLUP = true;
	const int MAX_READABLE_VALUE = 1023;
	const int THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS = 512;
};

#endif
