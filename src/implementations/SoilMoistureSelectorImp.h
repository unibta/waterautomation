#ifndef IMPLEMENTATIONS_SOILMOISTURESELECTORIMP_H_
#define IMPLEMENTATIONS_SOILMOISTURESELECTORIMP_H_

#include "Arduino.h"
#include "../interfaces/ISoilMoistureSelector.h"
#include "ReadableImp.h"

class SoilMoistureSelectorImp: virtual public ISoilMoistureSelector, public ReadableImp {
public:
	SoilMoistureSelectorImp();
	virtual ~SoilMoistureSelectorImp();

private:
	const uint8_t PIN_SIGNAL = A4;
	const bool PIN_SIGNAL_IS_ANALOG = true;
	const bool ENABLE_PIN_PULLUP = false;
	const int MAX_READABLE_VALUE = 1023;
	const int THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS = 5;
};

#endif
