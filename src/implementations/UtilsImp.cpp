/*
 * UtilsImp.cpp
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#include "UtilsImp.h"

UtilsImp::UtilsImp() {
	// TODO Auto-generated constructor stub

}

UtilsImp::~UtilsImp() {
	// TODO Auto-generated destructor stub
}

void UtilsImp::delay(unsigned long ms) {
	Arduino_h::delay(ms);
}

bool UtilsImp::isDigit(const char c) {
	return Arduino_h::isDigit(c);
}

unsigned long UtilsImp::millis() {
	return Arduino_h::millis();
}
