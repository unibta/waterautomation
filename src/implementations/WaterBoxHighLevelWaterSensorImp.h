#ifndef IMPLEMENTATIONS_WATERBOXHIGHLEVELWATERSENSORIMP_H_
#define IMPLEMENTATIONS_WATERBOXHIGHLEVELWATERSENSORIMP_H_

#include "Arduino.h"
#include "../interfaces/IWaterBoxHightLevelWaterSensor.h"
#include "ReadableImp.h"

class WaterBoxHighLevelWaterSensorImp: virtual public IWaterBoxHightLevelWaterSensor, public ReadableImp {
public:
	WaterBoxHighLevelWaterSensorImp();
	virtual ~WaterBoxHighLevelWaterSensorImp();

private:
	const uint8_t PIN_SIGNAL = A2;
	const bool PIN_SIGNAL_IS_ANALOG = true;
	const bool ENABLE_PIN_PULLUP = true;
	const int MAX_READABLE_VALUE = 1023;
	const int THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS = 512;
};

#endif
