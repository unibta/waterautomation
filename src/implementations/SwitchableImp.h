/*
 * SwitchableImp.h
 *
 *  Created on: Oct 16, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_SWITCHABLEIMP_H_
#define IMPLEMENTATIONS_SWITCHABLEIMP_H_

#include "../interfaces/ISwitchable.h"
#include <Arduino.h>

class SwitchableImp: virtual public ISwitchable {
public:
	SwitchableImp();
	SwitchableImp(const uint8_t pin);
	SwitchableImp(const uint8_t pin, bool inverseOutput);
	virtual ~SwitchableImp();

	void on();
	void off();
	void toggle();
	void dim(int dimVal);
	bool isOn();
	void setState(bool state);

protected:

	void setPin(const uint8_t pin);
	void setPin(const uint8_t pin, bool inverseOutput);

private:

	uint8_t pin;
	bool state = false;
	bool is_output_inverted = false;

	void updateOutputState();
};

#endif /* IMPLEMENTATIONS_SWITCHABLEIMP_H_ */
