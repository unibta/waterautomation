/*
 * LoggerImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_LOGGERIMP_H_
#define IMPLEMENTATIONS_LOGGERIMP_H_

#include "Arduino.h"
#include "../interfaces/ILogger.h"

class LoggerImp: virtual public ILogger {
public:
	LoggerImp();
	virtual ~LoggerImp();

	void logFreeRam();

	void log(const char c[]);
	void log(const char c);
	void log(int num);
	void log(long num);
	void log(double num);
	void log(bool b);

	void logln(const char c[]);
	void logln(const char c);
	void logln(int num);
	void logln(long num);
	void logln(double num);
	void logln(bool b);

private:
	const bool LOG_ENABLED = false;
	const int SERIAL_PORT = 9600;
};

#endif /* IMPLEMENTATIONS_LOGGERIMP_H_ */
