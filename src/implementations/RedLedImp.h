/*
 * IrrigationPumpOnLedImp.h
 *
 *  Created on: Oct 15, 2018
 *      Author: davidlima
 */

#ifndef IMPLEMENTATIONS_REDLEDIMP_H_
#define IMPLEMENTATIONS_REDLEDIMP_H_

#include "Arduino.h"

#include "../interfaces/IRedLed.h"
#include "SwitchableImp.h"

class RedLedImp: virtual public IRedLed, public SwitchableImp {
public:
	RedLedImp();
	virtual ~RedLedImp();

private:
	const uint8_t PIN_SIGNAL = 25;
};

#endif /* IMPLEMENTATIONS_REDLEDIMP_H_ */
