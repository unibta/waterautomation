#ifndef IMPLEMENTATIONS_DISPLAYIMP_H_
#define IMPLEMENTATIONS_DISPLAYIMP_H_

#include "Arduino.h"
#include "../interfaces/IDisplay.h"
#include "string.h"

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

class DisplayImp: public virtual IDisplay {
public:
	DisplayImp();
	virtual ~DisplayImp();

	void displayCisternInfo(const bool isLowLevelCisternTriggered, const bool isLowLevelWaterBoxTriggered, const bool isHighLevelWaterBoxTriggered, const bool isWaterBoxPumpActive, const char errorMessage[128]);
	void displayIrrigationInfo(const int soilMoistureValue, const int soilMoistureSetpoint, const bool isIrrigationPumpActive, const char errorMessage[128]);

private:

	char umidadeSolo[128];
	char umidadeMinima[128];

	char soilMoistureValueString[3];
	char soilMoistureSetpointString[3];

	const uint8_t PIN_SDA = 20;
	const uint8_t PIN_SLC = 21;

	Adafruit_SSD1306 * display;

	int getPercentageBySensorValue(int sensorValue);
};

#endif
