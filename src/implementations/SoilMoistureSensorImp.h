#ifndef IMPLEMENTATIONS_SOILMOISTURESENSORIMP_H_
#define IMPLEMENTATIONS_SOILMOISTURESENSORIMP_H_

#include "Arduino.h"
#include "../interfaces/ISoilMoistureSensor.h"
#include "ReadableImp.h"

class SoilMoistureSensorImp: virtual public ISoilMoistureSensor, public ReadableImp {
public:
	SoilMoistureSensorImp();
	virtual ~SoilMoistureSensorImp();
	int SoilMoistureSensorImp::readValue();
	int SoilMoistureSensorImp::readValuePercentage();

private:
	const uint8_t PIN_SIGNAL = A3;
	const bool PIN_SIGNAL_IS_ANALOG = true;
	const bool ENABLE_PIN_PULLUP = false;
	const int MAX_READABLE_VALUE = 1023;
	const int THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS = 512;
};

#endif
