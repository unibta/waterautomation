#include "SoilMoistureSensorImp.h"

SoilMoistureSensorImp::SoilMoistureSensorImp() {
	setupPinDefinitions(PIN_SIGNAL, PIN_SIGNAL_IS_ANALOG, THRESHOLD_VALUE_TO_SWITCH_TRIGGERED_STATUS, MAX_READABLE_VALUE, ENABLE_PIN_PULLUP);
}

SoilMoistureSensorImp::~SoilMoistureSensorImp() {

}

// Re-escrevendo pois o sensor está apenas ativando de 300 (humido) a 1023 (seco)
int SoilMoistureSensorImp::readValue() {
	int value = ReadableImp::readValue();
	int minValue = 500;
	if(value<minValue) value = minValue;
	value = map(value, minValue, 1023, 0, 1023);
	return 1023-value;
}

int SoilMoistureSensorImp::readValuePercentage() {
	int value = SoilMoistureSensorImp::readValue();
	float percentage = ((float)value/(float)1023.0)*100.0;
	return (int) round(percentage);
}
