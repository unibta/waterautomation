#include "ReadableImp.h"

ReadableImp::ReadableImp(const uint8_t pin, bool isAnalog, int thresholdValueToSwitchTriggeredStatus, int maxReadableValue, bool enablePullUp)
{
	setupPinDefinitions(pin, isAnalog, thresholdValueToSwitchTriggeredStatus, maxReadableValue, enablePullUp);
	applyPinDefinitions();

	if(isAnalog) {
		if(isPinMultiplexed) configureMultiplexedChannel();
		exponentialMovingAverageOutput = analogRead(pin);
	}
}

ReadableImp::ReadableImp() {

}

ReadableImp::~ReadableImp() {

}

void ReadableImp::setupPinDefinitions(const uint8_t pin, bool isAnalog, int thresholdValueToSwitchTriggeredStatus, int maxReadableValue, bool enablePullUp) {
	ReadableImp::pin = pin;
	ReadableImp::isAnalog = isAnalog;
	ReadableImp::thresholdValueToSwitchTriggeredStatus = thresholdValueToSwitchTriggeredStatus;
	ReadableImp::maxReadableValue = maxReadableValue;
	ReadableImp::enablePullUp = enablePullUp;
}

void ReadableImp::applyPinDefinitions() {
    setPin(pin, enablePullUp);
    setInputTypeAsAnalog(isAnalog);
    setThresholdValueToSwitchTriggeredStatus(thresholdValueToSwitchTriggeredStatus);
    setMaxReadableValue(maxReadableValue);
}

void ReadableImp::setPin(const uint8_t pin, bool enablePullUp) {
	ReadableImp::pin = pin;
	pinMode(pin, enablePullUp ? INPUT_PULLUP : INPUT);
}

int ReadableImp::readValue() {
	if(isPinMultiplexed) configureMultiplexedChannel();
	if(isAnalog) {
		analogSensorValue = analogRead(pin);
		exponentialMovingAverageOutput = (EXPONENTIAL_MOVING_AVERAGE_ALPHA*(float)analogSensorValue) + ((1-EXPONENTIAL_MOVING_AVERAGE_ALPHA)*(float)exponentialMovingAverageOutput);
		return (int)round(exponentialMovingAverageOutput);
	} else {
		return digitalRead(pin);
	}
}

int ReadableImp::readValuePercentage() {
	if(isPinMultiplexed) configureMultiplexedChannel();
	int value = readValue();
	float percentage = ((float)value/(float)maxReadableValue)*100.0;
	return (int) round(percentage);
}

bool ReadableImp::isTriggered() {
	if(isPinMultiplexed) configureMultiplexedChannel();
	if(isAnalog){
		analogSensorValue = readValue();
		if( analogSensorValue >= 1023 || analogSensorValue > (thresholdValueToSwitchTriggeredStatus+THRESHOLD_MARGIN_TO_AVOID_ANALOG_TRIGGER_FLUCTUATION)) {
			currentTriggeredStatusToSolveAnalogFluctuationTriggerFluctuation = true;
		} else if( analogSensorValue <= 1 || analogSensorValue < (thresholdValueToSwitchTriggeredStatus-THRESHOLD_MARGIN_TO_AVOID_ANALOG_TRIGGER_FLUCTUATION) ) {
			currentTriggeredStatusToSolveAnalogFluctuationTriggerFluctuation = false;
		}
		return currentTriggeredStatusToSolveAnalogFluctuationTriggerFluctuation;
	} else {
		return (readValue()>thresholdValueToSwitchTriggeredStatus);
	}
}

void ReadableImp::setInputTypeAsAnalog(bool value) {
	isAnalog = value;
}

void ReadableImp::setThresholdValueToSwitchTriggeredStatus(int value) {
	thresholdValueToSwitchTriggeredStatus = value;
}

void ReadableImp::setMaxReadableValue(int value) {
	maxReadableValue = value;
}

void ReadableImp::enableABCMultiplexMode(const uint8_t chanelAPin, const uint8_t chanelBPin, const uint8_t chanelCPin, const uint8_t chanelSelectionBits[3]){
	ReadableImp::chanelAPin = chanelAPin;
	ReadableImp::chanelBPin = chanelBPin;
	ReadableImp::chanelCPin = chanelCPin;
	ReadableImp::chanelSelectionBits[0] = chanelSelectionBits[0];
	ReadableImp::chanelSelectionBits[1] = chanelSelectionBits[1];
	ReadableImp::chanelSelectionBits[2] = chanelSelectionBits[2];

	pinMode(chanelAPin, OUTPUT);
	pinMode(chanelBPin, OUTPUT);
	pinMode(chanelCPin, OUTPUT);

	isPinMultiplexed = true;
}

void ReadableImp::configureMultiplexedChannel() {
	if(isPinMultiplexed &&
		!(  digitalRead(chanelAPin) == chanelSelectionBits[2] &&
			digitalRead(chanelBPin) == chanelSelectionBits[1] &&
			digitalRead(chanelCPin) == chanelSelectionBits[0]))
	{
		digitalWrite(chanelAPin, chanelSelectionBits[2]);
		digitalWrite(chanelBPin, chanelSelectionBits[1]);
		digitalWrite(chanelCPin, chanelSelectionBits[0]);
		applyPinDefinitions();
	}
}
