#include "Irrigador.h"

constexpr char Irrigador::SYSTEM_STATUS_OK[];
constexpr char Irrigador::SYSTEM_STATUS_LOW_WATER_LEVEL[];

Irrigador::Irrigador(ISoilMoistureSelector * soilMoistureSelector, IIrrigationPumpRelay* irrigationPumpRelay, ILogger* logger, ISoilMoistureSensor* soilMoistureSensor, IUtils* utils, IWaterBoxLowLevelWaterSensor* waterBoxLowLevelWaterSensor) {
	Irrigador::irrigationPumpRelay = irrigationPumpRelay;
	Irrigador::logger = logger;
	Irrigador::soilMoistureSensor = soilMoistureSensor;
	Irrigador::soilMoistureSelector = soilMoistureSelector;
	Irrigador::utils = utils;
	Irrigador::waterBoxLowLevelWaterSensor = waterBoxLowLevelWaterSensor;

	strcpy( systemInfo.errorMessage, SYSTEM_STATUS_OK );
	state = idle_analizing_sensors;
}

Irrigador::~Irrigador() {

}

void Irrigador::loop() {

	readSensorsToSystemInfo();

	soilMoistureSensor->setThresholdValueToSwitchTriggeredStatus(systemInfo.soilMoistureSelector);

	switch(state) {
		case idle_analizing_sensors: runIdleAnalizingSensorsState(); break;
		case pumping_water: runPumpingWaterState(); break;
		case alert_waterbox_low_level: runAlertWaterboxLowLevelState(); break;
	}
}

Irrigador::SystemInfo Irrigador::getSystemInfo() {
	return systemInfo;
}

void Irrigador::readSensorsToSystemInfo() {
	systemInfo.soilMoistureSelector = soilMoistureSelector->readValue();
	systemInfo.soilMoistureValue = soilMoistureSensor->readValue();
	systemInfo.isLowLevelWaterBoxTriggered = waterBoxLowLevelWaterSensor->readValue();
	systemInfo.isIrrigationPumpActive = irrigationPumpRelay->isOn();
}

void Irrigador::runIdleAnalizingSensorsState() {
	if(isAlertWaterboxLowLevelEnterConditionReached()) {
		state = alert_waterbox_low_level;
	}

	else if(isPumpingWaterEnterConditionReached()) {
		state = pumping_water;
	}
}

void Irrigador::runPumpingWaterState() {
	if(isAlertWaterboxLowLevelEnterConditionReached()) {
		irrigationPumpRelay->off();
		state = alert_waterbox_low_level;
	}

	else if(isPumpingWaterExitConditionReached()) {
		irrigationPumpRelay->off();
		state = idle_analizing_sensors;
	}

	else if (!irrigationPumpRelay->isOn()) {
		irrigationPumpRelay->on();
	}
}

void Irrigador::runAlertWaterboxLowLevelState() {
	if(isAlertWaterboxLowLevelExitConditionReached()) {
		systemInfo.hasErrors = false;
		strcpy( systemInfo.errorMessage, SYSTEM_STATUS_OK );
		state = idle_analizing_sensors;
	}

	else {
		if(!systemInfo.hasErrors){
			systemInfo.hasErrors = true;
			strcpy( systemInfo.errorMessage, SYSTEM_STATUS_LOW_WATER_LEVEL );
		}
	}
}


bool Irrigador::isPumpingWaterEnterConditionReached() {
	return systemInfo.soilMoistureSelector > systemInfo.soilMoistureValue + THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION;
}

bool Irrigador::isAlertWaterboxLowLevelEnterConditionReached() {
	return waterBoxLowLevelWaterSensor->isTriggered();
}



bool Irrigador::isPumpingWaterExitConditionReached() {
	// se o seletor for theshold a menos que o sensor OU se o seletor for menor ou igual ao theshold
	return (
		systemInfo.soilMoistureSelector <= systemInfo.soilMoistureValue - THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION || // condiçao onde a humidade ultrapassa o seletor
		systemInfo.soilMoistureSelector <= THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION || // condição onde o seletor é praticamente zero (off)
		systemInfo.soilMoistureValue >= 1023 - THRESHOLD_MARGIN_TO_AVOID_ANALOG_FLUCTUATION // condição onde a humidade atingiou o 100%, (nao teria como ultrapassar o seletor)
	);
}

bool Irrigador::isAlertWaterboxLowLevelExitConditionReached() {
	return !waterBoxLowLevelWaterSensor->isTriggered();
}
